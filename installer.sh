#!/bin/bash

cd /tmp/

if [ `getconf LONG_BIT` = "64" ]
then
    wget -O teamviewer_amd64.deb https://download.teamviewer.com/download/linux/teamviewer_amd64.deb
    sudo apt install /tmp/teamviewer_amd64.deb
else
    wget -O teamviewer_i386.deb https://download.teamviewer.com/download/linux/teamviewer_i386.deb
    sudo apt install /tmp/teamviewer_i386.deb
fi

teamviewer